#include <iostream>
#include <ANN.h>
using namespace std;
using namespace ANN;

int main()
{
	// ������� �������(���������) � �������� ������
	vector<vector<float>>vec_input_data, vec_output_data;
	vector<float> vec_output_ann;

	cout << "hello ANN!" << endl;
	cout << GetTestString().c_str() << endl;

	auto ann = ANN::CreateNeuralNetwork();
	ann->Load("../Output data.txt");
	cout << ann->GetType().c_str() << endl;

	ANN::LoadData("../Training data.txt", vec_input_data, vec_output_data);

	cout << "Test network" << endl << "ANN" << "\t\t\t" << "REAL" << endl;

	for (int i = 0; i < vec_input_data.size(); i++)
	{
		vec_output_ann.clear();
		vec_output_ann = ann->Predict(vec_input_data[i]);
		
		for (int j = 0; j < vec_output_data[i].size(); j++)
		{
			cout << vec_output_ann[j] << " \t\t\t" << vec_output_data[i][j] << endl;
		}
	}

	system("pause");
	return 0;
}