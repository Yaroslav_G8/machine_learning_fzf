#include <iostream>
#include <ANN.h>
using namespace std;
using namespace ANN;

int main()
{
	cout << "hello ANN!" << endl;
	cout << GetTestString().c_str() << endl;

	// ������� �������(���������) � �������� ������
	vector<vector<float>>vec_input_data, vec_output_data;

	ANN::LoadData("../Training data.txt", vec_input_data, vec_output_data);

	// ������ ������������ ����
	vector<int> vec_config_network;
	vec_config_network = { 2, 10, 10, 1 };

	auto ann = ANN::CreateNeuralNetwork(vec_config_network);
	cout << GetTestString().c_str() << endl;
	cout << ann->GetType().c_str() << endl;

	ann->MakeTrain(vec_input_data, vec_output_data, 100000, 0.1, 0.1, true);

	ann->Save("../Output data.txt");

	system("pause");
	return 0;
}